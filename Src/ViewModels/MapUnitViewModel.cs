﻿// Copyright 2018 Jeffrey Jones
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

namespace CinC.App.ViewModels
{
    using CinC.App.Models;
    using Windows.Devices.Geolocation;

    public class MapUnitViewModel : NotificationBase<MapUnit>
    {
        public MapUnitViewModel(MapUnit mapUnit = null) : base(mapUnit)
        {
        }

        public string Id
        {
            get { return notification.Id; }
            set { this.SetProperty(notification.Id, value, () => notification.Id = value); }
        }

        public Geopoint Location
        {
            get { return notification.Location; }
            set { this.SetProperty(notification.Location, value, () => notification.Location = value); }
        }

        public string Name
        {
            get { return notification.Name; }
            set { this.SetProperty(notification.Name, value, () => notification.Name = value); }
        }

        public decimal Altitude
        {
            get { return notification.Altitude; }
            set { this.SetProperty(notification.Altitude, value, () => notification.Altitude = value); }
        }

        public int Heading
        {
            get { return notification.Heading; }
            set { this.SetProperty(notification.Heading, value, () => notification.Heading = value); }
        }

        public string Type
        {
            get { return notification.Type; }
            set { this.SetProperty(notification.Type, value, () => notification.Type = value); }
        }

        public string Coalition
        {
            get { return notification.Coalition; }
            set { this.SetProperty(notification.Coalition, value, () => notification.Coalition = value); }
        }

        public string Country
        {
            get { return notification.Country; }
            set { this.SetProperty(notification.Country, value, () => notification.Country = value); }
        }

        public string Group
        {
            get { return notification.Group; }
            set { this.SetProperty(notification.Group, value, () => notification.Group = value); }
        }

        public string Color
        {
            get { return notification.Color; }
            set { this.SetProperty(notification.Color, value, () => notification.Color = value); }
        }

        public string Pilot
        {
            get { return notification.Pilot; }
            set { this.SetProperty(notification.Pilot, value, () => notification.Pilot = value); }
        }

        public string Icon
        {
            get { return notification.Icon; }
            set { this.SetProperty(notification.Icon, value, () => notification.Icon = value); }
        }
    }
}
