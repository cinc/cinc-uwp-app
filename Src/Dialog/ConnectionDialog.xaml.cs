﻿// Copyright 2018 Jeffrey Jones
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//
// The Content Dialog item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace CinC.App.Dialog
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Threading;
    using System.Threading.Tasks;
    using CinC.App.Telemetry;
    using Windows.UI.Xaml.Controls;

    public enum ConnectionResult
    {
        /// <summary>
        /// We have successfully connected to the server.
        /// </summary>
        Connected,

        /// <summary>
        /// We have not successfully connected to the server.
        /// </summary>
        NotConnected,
    }

    /// <summary>
    /// Provides implementation for for the Network Connection Dialog.
    /// </summary>
    public sealed partial class ConnectionDialog : ContentDialog
    {
        public ConnectionDialog()
        {
            this.InitializeComponent();
        }

        public ConnectionResult Result { get; private set; }

        public Client TelemetryClient { get; private set; }

        private async void ContentDialog_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            args.Cancel = true;
            this.Result = ConnectionResult.NotConnected;

            if (string.IsNullOrEmpty(ServerIp.Text))
            {
                Message.Text = "Server IP is required";
            }
            else if (string.IsNullOrEmpty(CommandPort.Text))
            {
                Message.Text = "Command Port is required";
            }
            else if (string.IsNullOrEmpty(TelemetryPort.Text))
            {
                Message.Text = "Telemetry Port is required";
            }
            else
            {
                this.TelemetryClient = new Client();
                Message.Text = "Connecting";
                try
                {
                    var connectionSuccessful = await this.TelemetryClient.ConnectAsync(ServerIp.Text, TelemetryPort.Text);
                    if (connectionSuccessful == true)
                    {
                        args.Cancel = false;
                        this.Result = ConnectionResult.Connected;
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.ToString());
                    Message.Text = e.Message.Split('\n')[0];
                }
            }
        }

        private void ContentDialog_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            this.Result = ConnectionResult.NotConnected;
        }
    }
}
